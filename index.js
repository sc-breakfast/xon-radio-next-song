#!/usr/bin/env node
const http = require('http');
const fs = require('fs');
const path = require('path');
const morgan = require('morgan')

const argv = require('yargs') // eslint-disable-line
  .option('file', {
    describe: 'path to a file to read lines from',
  })
  .option('port', {
    describe: 'port to listen on',
    default: 8080,
  })
  .option('host', {
    describe: 'host to listen on',
    default: '127.0.0.1',
  })
  .demandOption(['file'])
  .help()
  .version()
  .argv;

const CheckFileExistance = () => {
  if (fs.existsSync(file)) return;
  console.error(`Cannot find file ${argv.file}`);
  process.exit(1);
}

const LoadFileContents = () =>
  fs.readFileSync(file, 'utf8')
    .split('\n')
    .filter(line => line.length > 0);

const file = path.resolve(process.cwd(), argv.file);
CheckFileExistance();
let list = LoadFileContents();
console.info(`File read from ${argv.file} (${list.length} lines)`);

fs.watch(file, eventType => {
  if (eventType !== 'change') return;
  CheckFileExistance();
  list = LoadFileContents();
  console.info(`File reloaded from ${argv.file} (${list.length} lines)`);
});

const logger = morgan('combined');
http.createServer((req, res) => {
  logger(req, res, () => {
    res.setHeader('Content-Type', 'text/plain');
    res.write(list[Math.floor(Math.random() * list.length)]);
    res.end();
  });
}).listen(argv.port, argv.host);
console.info(`Listening on http://${argv.host}:${argv.port}/`);
